<?php

namespace Molengo\Test;

use \Molengo\DateTime\Time;

/**
 * @coversDefaultClass \Molengo\DateTime\Time
 */
class TimeTest extends \Molengo\Test\TestCase
{

    /**
     * Test random function
     *
     * @covers ::isTime
     */
    public function testIsTime()
    {
        $result = Time::isTime('12:15:45');
        $this->assertEquals(true, $result);

        $result = Time::isTime('23:59:59');
        $this->assertEquals(true, $result);

        $result = Time::isTime('');
        $this->assertEquals(false, $result);

        $result = Time::isTime('2015-08-01 10:20:30');
        $this->assertEquals(false, $result);

        $result = Time::isTime(null);
        $this->assertEquals(false, $result);

        $result = Time::isTime(false);
        $this->assertEquals(false, $result);
    }
}
