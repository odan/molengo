<?php

namespace Molengo\Test\Model;

class AppModel extends \Molengo\Model\BaseModel
{

    /**
     * Returns CRUD model
     *
     * @return \Molengo\Model\CrudModel
     */
    public function crud()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new \Molengo\Model\CrudModel($this->db);
            $instance->setUserId(1);
        }
        return $instance;
    }

}
