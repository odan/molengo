<?php

namespace Molengo\Test;

use Molengo\Db\DbMySqlGeneralLog;

/**
 * @coversDefaultClass \Molengo\Db\DbMySqlGeneralLog
 */
class DbMySqlGeneralLogTest extends DbMySqlIlluminateTestBase
{

    /**
     * Test create object
     *
     * @return void
     */
    public function testInstance()
    {
        $this->assertTrue(class_exists('\Molengo\Db\DbMySqlGeneralLog'));

        $pdo = $this->getDb()->getConnection()->getPdo();
        $object = new DbMySqlGeneralLog($pdo);
        $this->assertInstanceOf('\Molengo\Db\DbMySqlGeneralLog', $object);
    }

    /**
     * Test general log
     *
     * @covers ::start
     * @covers ::status
     * @covers ::count
     * @covers ::sql
     * @covers ::dml
     */
    public function testGeneralLog()
    {
        $db = $this->getDb();
        $log = $db->getGeneralLog()->start(true);

        $this->createTestTable();
        $db->table('test')->get();

        $actual = $log->status();
        $this->assertSame(true, $actual);

        $actual = $log->count();
        $this->assertSame(true, $actual >= 17);

        $actual = $log->sql();
        $this->assertSame(true, !empty($actual[0]));
        $this->assertSame(true, count($actual) >= 20);

        $actual = $log->dml();
        $this->assertSame(true, !empty($actual[0]['argument']));

        $log->stop(true);
    }
}
