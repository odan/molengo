<?php

namespace Molengo\Test;

use Molengo\Container\Container;

/**
 * @coversDefaultClass \Molengo\Container\Container
 */
class ContainerTest extends \Molengo\Test\TestCase
{

    /**
     * Test
     */
    public function testContainer()
    {
        $obj = new Container();

        $obj->member = 1;
        $result = $obj->member;
        $this->assertSame(1, $result);

        $result = property_exists($obj, 'member');
        $this->assertSame(true, $result);

        unset($obj->member);
        $result = property_exists($obj, 'member');
        $this->assertSame(false, $result);

        $obj->test = function() {
            return "Hello world";
        };
        $result = $obj->test();
        $this->assertSame("Hello world", $result);

        $result = method_exists($obj, 'test');
        $this->assertSame(false, $result);

        $result = property_exists($obj, 'test');
        $this->assertSame(true, $result);

        $result = gettype($obj->test);
        $this->assertSame('object', $result);

        unset($obj->test);
        $result = property_exists($obj, 'test');
        $this->assertSame(false, $result);

        $obj->sub = new Container();
        $obj->sub->sub2 = 1;
        $result = $obj->sub->sub2;
        $this->assertSame(1, $result);
    }
}
