<?php

if (php_sapi_name() == "cli") {
    return;
}

require_once __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');

// time and memory limits
set_time_limit(300);
ini_set('memory_limit', '-1');

$request = Symfony\Component\HttpFoundation\Request::createFromGlobals();
$testSuite = new \Molengo\Config\App();
$testSuite = new \Molengo\Test\TestSuite();
$testSuite->setTestDir(__DIR__);
$testSuite->setTemplate(__DIR__ . '/html/index.html.php');
$testSuite->setCoverage(!empty($request->query->get('coverage')));
$testSuite->setNamespace('\Molengo\\Test\\');
$testSuite->setCoverageDir(__DIR__ . '/../build/coverage');
$testSuite->addDirectoryToWhitelist(__DIR__ . '/../src');
$testSuite->addDirectoryToBlacklist(__DIR__ . '/../tests');
$testSuite->addDirectoryToBlacklist(__DIR__ . '/../vendor');
$testSuite->addDirectoryToBlacklist(__DIR__ . '/../build');
$testSuite->run();
