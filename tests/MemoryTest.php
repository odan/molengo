<?php

namespace Molengo\Test;

/**
 * @coversDefaultClass \Molengo\Test\TestCase
 */
class MemoryTest extends \Molengo\Test\TestCase
{

    /**
     * Test
     *
     * @covers ::addMemoryUsage
     * @covers ::getMemoryUsage
     * @covers ::formatByte
     */
    public function testAddMemoryUsageA()
    {
        // Set memory usage before loop
        $this->addMemoryUsage('Before Loop');

        $arr = array();
        for ($i = 0; $i < 3999; $i++) {
            array_push($arr, str_repeat('a', $i));
        }
        // Set memory usage after loop
        $this->addMemoryUsage('After Loop');

        $this->assertEquals(true, true);
    }

    /**
     * Test
     *
     * @covers ::addMemoryUsage
     */
    public function testAddMemoryUsageB()
    {
        // Set memory usage before loop
        $this->addMemoryUsage('Before Loop B');

        $arr = array();
        for ($i = 0; $i < 9999; $i++) {
            array_push($arr, str_repeat('a', $i));
        }

        $a = array();
        for ($i = 0; $i < 10000; $i++) {
            array_push($a, 'test');
        }

        $this->addMemoryUsage('After Loop B');
        $this->assertEquals(true, true);
    }

    /**
     * Test
     *
     * @covers ::addMemoryUsage
     */
    public function testAddMemoryUsageC()
    {
        $this->addMemoryUsage();

        $arr = array();
        for ($i = 0; $i < 100; $i++) {
            array_push($arr, str_repeat('zz', $i));
        }

        $this->assertEquals(true, true);
        $this->addMemoryUsage();
    }
}
