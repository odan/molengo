<?php

namespace Molengo\Test;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

/**
 * Base test
 */
class DbMySqlTestBase extends \Molengo\Test\TestCase
{

    protected static $dbConfig = array();
    protected static $logDir;

    /**
     * Constructor
     *
     * @param string $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        static::$dbConfig = array(
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'database' => 'molengo_test',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        );
    }

    /**
     * Run once for each test method
     */
    protected function setUp()
    {
        parent::setUp();

        static::$logDir = __DIR__ . '/../build/test/logs';
        if (!file_exists(static::$logDir)) {
            mkdir(static::$logDir, 0775, true);
        }
        static::$logDir = realpath(static::$logDir);
    }

    /**
     * Return logger
     *
     * @return Logger
     */
    protected static function getLogger()
    {
        // Create a log channel
        $log = new Logger('mysqldbtest');
        $level = Logger::DEBUG;
        $filename = static::$logDir . '/log_mysql.txt';
        $handler = new RotatingFileHandler(
            $filename, 0, $level, true, 0775
        );
        $log->pushHandler($handler);
        return $log;
    }

    /**
     * Empty test
     *
     * @return void
     */
    public function testEmpty()
    {

    }
}
