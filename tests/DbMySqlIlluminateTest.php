<?php

namespace Molengo\Test;

use Molengo\DateTime\Time;

/**
 * @coversDefaultClass \Molengo\Db\DbMySqlIlluminate
 */
class DbMySqlIlluminateTest extends DbMySqlIlluminateTestBase
{

    /**
     * Test create object
     *
     * @return void
     */
    public function testInstance()
    {
        $this->assertTrue(class_exists('\Molengo\Db\DbMySqlIlluminate'));
        $this->assertInstanceOf('\Molengo\Db\DbMySqlIlluminate', static::getDb());
    }

    /**
     * Test table
     */
    public function testTable()
    {
        $db = static::getDb();
        $this->createTestTable();

        $rows = [
            ['keyname' => 'info@example.com', 'keyvalue' => 0],
            ['keyname' => 'info2@example.com', 'keyvalue' => 1],
            ['keyname' => 'empty', 'keyvalue' => null]
        ];
        $db->table('test')->insert($rows);

        $users = $db->table('test')->select('keyname', 'keyvalue')->get();
        $this->assertEquals($rows, $users);
    }

    /**
     * Test events
     *
     */
    public function testEvents()
    {
        $this->createTestTable();

        $db = $this->getDb();

        $queryCalled = 0;
        $beforeSelect = 0;
        $afterSelect = 0;
        $that = $this;

        $event = $db->getConnection()->getEventDispatcher();
        $event->listen('illuminate.query', function($event, $payload) use(&$queryCalled) {
            $queryCalled++;
        });

        $event->listen('query.beforeSelect', function($event, $query) use(&$that, &$beforeSelect ) {

            $that->assertInstanceOf('\Molengo\Db\DbMySqlIlluminateQueryBuilder', $query);

            $beforeSelect++;

            // Add soft delete condition
            $query->whereNull('deleted');

            // Return false to stop the query
        });

        $event->listen('query.afterSelect', function($event, $query) use (&$that, &$afterSelect) {
            $that->assertInstanceOf('\Molengo\Db\DbMySqlIlluminateQueryBuilder', $query);
            $afterSelect++;
        });

        $db->table('test')->insert([
            ['keyname' => 'info@example.com', 'keyvalue' => 0, 'deleted' => null],
            ['keyname' => 'mail@example.com', 'keyvalue' => 1, 'deleted' => null],
            ['keyname' => 'test', 'keyvalue' => null, 'deleted' => Time::now()]
        ]);

        // Exec some queries
        $db->table('test')->get();
        $db->table('test')->where('id', 1)->first();

        $this->assertSame(3, $queryCalled);
        $this->assertSame(2, $beforeSelect);
        $this->assertSame(2, $afterSelect);
    }
}
