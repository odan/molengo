<?php

namespace Molengo\Test;

/**
 * @coversDefaultClass \Molengo\Mail\MessagePhpMailer
 */
class MessagePhpMailerTest extends \Molengo\Test\TestCase
{

    /**
     * Test
     *
     * @covers ::setDate
     * @covers ::getDate
     */
    public function testPhpMailerDate()
    {
        $email = new \Molengo\Mail\MessagePhpMailer();

        $time = time();
        $email->setDate($time);
        $time2 = $email->getDate();
        $this->assertSame($time2, $time);
    }

    /**
     * Test
     *
     * @covers ::setTo
     * @covers ::getTo
     */
    public function testPhpMailerTo()
    {
        $email = new \Molengo\Mail\MessagePhpMailer();

        $email->setTo('info@example.com');
        $actual = $email->getTo();
        $expected = array(
            0 =>
            array(
                0 => 'info@example.com',
                1 => '',
            ),
        );
        $this->assertSame($expected, $actual);

        $email->setTo('mail@example.com', 'Firstname Lastname');
        $actual = $email->getTo();
        $expected = array(
            array(
                0 => 'info@example.com',
                1 => '',
            ),
            array(
                0 => 'mail@example.com',
                1 => 'Firstname Lastname',
            ),
        );
        $this->assertSame($expected, $actual);
    }

    /**
     * Test
     *
     * @covers ::setConfig
     * @covers ::getConfigMapping
     */
    public function testSetConfig()
    {
        $email = new \Molengo\Mail\MessagePhpMailer();

        $config = array(
            'type' => 'smtp',
            'host' => '127.0.0.1',
            'helo' => '127.0.0.1',
            'port' => '465',
            'secure' => '',
            'username' => 'mail@example.com',
            'password' => '',
            'from' => 'mail@example.com',
            'from_name' => 'my name',
            'to' => 'mail@example.com',
            'cc' => 'bcc@example.com',
            'bcc' => 'bcc@example.com',
            'charset' => 'UTF-8',
            'smtpauth' => 'SMTPAuth',
            'authtype' => 'authtype',
            'nada' => '1'
        );

        $actual = $email->setConfig($config);
        $this->assertTrue($actual);
    }
}
