<?php

namespace Molengo\Test;

/**
 * UtilTest
 */
class UtilTest extends \Molengo\Test\TestCase
{

    /**
     * Test gh function
     *
     * @covers ::gh
     */
    public function testGh()
    {
        $result = gh(null);
        $this->assertEquals('', $result);

        $result = gh('');
        $this->assertEquals('', $result);

        $result = gh(' ');
        $this->assertEquals(' ', $result);

        $result = gh('abcdefghijklmnopqrstuvwxyz');
        $this->assertEquals('abcdefghijklmnopqrstuvwxyz', $result);

        $result = gh('01234567890');
        $this->assertEquals('01234567890', $result);

        $result = gh('&"\'<>');
        $this->assertEquals('&amp;&quot;&#039;&lt;&gt;', $result);

        $result = gh('öäü#+!"§$%&/()=?´\\~<>|ÿ');
        $this->assertEquals('&ouml;&auml;&uuml;#+!&quot;&sect;$%&amp;/()=?&acute;\~&lt;&gt;|&yuml;', $result);

        $result = gh("\t\r\n\0");
        $this->assertEquals('&#9;&#13;&#10;&#0;', $result);

        $result = gh('öäü@€È/\"&\'<>');
        $this->assertEquals('&ouml;&auml;&uuml;@&euro;&Egrave;/\&quot;&amp;&#039;&lt;&gt;', $result);

        $result = gh('¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ');
        $this->assertEquals('&iexcl;&cent;&pound;&curren;&yen;&brvbar;&sect;&uml;&copy;&ordf;&laquo;&not;&shy;&reg;&macr;&deg;&plusmn;&sup2;&sup3;&acute;&micro;&para;&middot;&cedil;&sup1;&ordm;&raquo;&frac14;&frac12;&frac34;&iquest;&Agrave;&Aacute;&Acirc;&Atilde;&Auml;&Aring;&AElig;&Ccedil;&Egrave;&Eacute;&Ecirc;&Euml;&Igrave;&Iacute;&Icirc;&Iuml;&ETH;&Ntilde;&Ograve;&Oacute;&Ocirc;&Otilde;&Ouml;&times;&Oslash;&Ugrave;&Uacute;&Ucirc;&Uuml;&Yacute;&THORN;&szlig;&agrave;&aacute;&acirc;&atilde;&auml;&aring;&aelig;&ccedil;&egrave;&eacute;&ecirc;&euml;&igrave;&iacute;&icirc;&iuml;&eth;&ntilde;&ograve;&oacute;&ocirc;&otilde;&ouml;&divide;&oslash;&ugrave;&uacute;&ucirc;&uuml;&yacute;&thorn;&yuml;', $result);

        $result = gh('ƒΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρςστυφχψωϑϒϖ•…′″‾⁄℘ℑℜ™ℵ←↑→↓↔↵⇐⇑⇒⇓⇔∀∂∃∅∇∈∉∋∏∑−∗√∝∞∠∧∨∩∪∫∴∼≅≈≠≡≤≥⊂⊃⊄⊆⊇⊕⊗⊥⋅⌈⌉⌊⌋〈〉◊♠♣♥♦�');
        $this->assertEquals('&fnof;&Alpha;&Beta;&Gamma;&Delta;&Epsilon;&Zeta;&Eta;&Theta;&Iota;&Kappa;&Lambda;&Mu;&Nu;&Xi;&Omicron;&Pi;&Rho;&Sigma;&Tau;&Upsilon;&Phi;&Chi;&Psi;&Omega;&alpha;&beta;&gamma;&delta;&epsilon;&zeta;&eta;&theta;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&rho;&sigmaf;&sigma;&tau;&upsilon;&phi;&chi;&psi;&omega;&thetasym;&upsih;&piv;&bull;&hellip;&prime;&Prime;&oline;&frasl;&weierp;&image;&real;&trade;&alefsym;&larr;&uarr;&rarr;&darr;&harr;&crarr;&lArr;&uArr;&rArr;&dArr;&hArr;&forall;&part;&exist;&empty;&nabla;&isin;&notin;&ni;&prod;&sum;&minus;&lowast;&radic;&prop;&infin;&ang;&and;&or;&cap;&cup;&int;&there4;&sim;&cong;&asymp;&ne;&equiv;&le;&ge;&sub;&sup;&nsub;&sube;&supe;&oplus;&otimes;&perp;&sdot;&lceil;&rceil;&lfloor;&rfloor;&lang;&rang;&loz;&spades;&clubs;&hearts;&diams;&#65533;', $result);
    }

    /**
     * Test blank function
     *
     * @covers ::blank
     */
    public function testBlank()
    {
        $boolResult = blank('');
        $this->assertTrue($boolResult);

        $boolResult = blank(false);
        $this->assertTrue($boolResult);

        $boolResult = blank(true);
        $this->assertFalse($boolResult);

        $boolResult = blank('0');
        $this->assertFalse($boolResult);

        $boolResult = blank('0.00');
        $this->assertFalse($boolResult);

        $boolResult = blank(0);
        $this->assertFalse($boolResult);

        $boolResult = blank(array());
        $this->assertTrue($boolResult);
    }

    /**
     * Test interpolate function
     *
     * @covers ::interpolate
     * @covers ::i
     */
    public function testInterpolate()
    {
        $result = interpolate('');
        $this->assertEquals($result, '');

        $result = interpolate('Test');
        $this->assertEquals($result, 'Test');

        $result = interpolate('User {username} created', array('username' => 'Max'));
        $this->assertEquals($result, 'User Max created');

        $result = i('User {username} created', array('username' => 'Max'));
        $this->assertEquals($result, 'User Max created');
    }
}
