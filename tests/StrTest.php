<?php

namespace Molengo\Test;

use \Molengo\Util\Str;

/**
 * @coversDefaultClass \Molengo\Util\Str
 */
class StrTest extends \Molengo\Test\TestCase
{

    /**
     * Test truncate function
     *
     * @covers ::truncate
     */
    public function testTruncate()
    {

    }

    /**
     * Test uuid function
     *
     * @covers ::uuid
     */
    public function testUuid()
    {

    }

    /**
     * Test encodeUtf8 function
     *
     * @covers ::encodeUtf8
     */
    public function testEncodeUtf8()
    {

    }

    /**
     * Test encodeJson function
     *
     * @covers ::encodeJson
     */
    public function testEncodeJson()
    {

    }

    /**
     * Test decodeJson function
     *
     * @covers ::decodeJson
     */
    public function testDecodeJson()
    {

    }

    /**
     * Test urlTitle function
     *
     * @covers ::urlTitle
     * @covers ::charMapUtf8Array
     * @covers ::uord
     */
    public function testUrlTitle()
    {

    }

    /**
     * Test encodIso function
     *
     * @covers ::encodeIso
     */
    public function testEncodeIso()
    {

    }

    /**
     * Test uchr function
     *
     * @covers ::uchr
     */
    public function testUchr()
    {

    }

    /**
     * Test uord function
     *
     * @covers ::uord
     */
    public function testUord()
    {

    }

    /**
     * Test isEmail function
     *
     * @covers ::isEmail
     */
    public function testIsEmail()
    {

    }

    /**
     * Test trim function
     *
     * @covers ::trim
     */
    public function testTrim()
    {

    }

    /**
     * Test random function
     *
     * @covers \Molengo\Util\Str::random
     */
    public function testRandomString()
    {
        $result = Str::random(0);
        $this->assertEquals(0, strlen($result));

        $result = Str::random(10);
        $this->assertEquals(10, strlen($result));

        $result = Str::random(255);
        $this->assertEquals(255, strlen($result));

        $result = Str::random(12, true, true, true, true);
        $this->assertEquals(12, strlen($result));
    }
}
