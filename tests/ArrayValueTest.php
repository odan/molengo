<?php

namespace Molengo\Test;

use \Molengo\Map\ArrayValue;
use \Molengo\Test\TestCase;

/**
 * @coversDefaultClass \Molengo\Map\ArrayValue
 */
class ArrayValueTest extends TestCase
{

    protected function getTestArray()
    {
        $arr = array();
        $arr['key'] = 'value';
        $arr['key2']['sub1'] = 'value2';
        $arr['key3']['sub1']['sub2'] = 'value3';
        return $arr;
    }

    /**
     * Test get function
     *
     * @covers ::get
     */
    public function testGet()
    {
        $arr = $this->getTestArray();

        $result = ArrayValue::get($arr, ['key']);
        $this->assertEquals('value', $result);

        $result = ArrayValue::get($arr, ['key2', 'sub1']);
        $this->assertEquals('value2', $result);

        $result = ArrayValue::get($arr, ['key3', 'sub1', 'sub2']);
        $this->assertEquals('value3', $result);

        $result = ArrayValue::get($arr, ['key4', 'sub1', 'sub2'], 'defaultvalue');
        $this->assertEquals('defaultvalue', $result);

        $arr = null;
        $result = ArrayValue::get($arr, ['key4', 'sub1', 'sub2'], 'defaultvalue');
        $this->assertEquals('defaultvalue', $result);
    }

    /**
     * Test set function
     *
     * @covers ::set
     */
    public function testSet()
    {
        $arr = $this->getTestArray();

        // 1 dimension
        $keys = explode('.', 'key');
        $result = ArrayValue::set($arr, $keys, 'new');
        $expected = $arr;
        $expected['key'] = 'new';
        $this->assertEquals($expected, $result);

        $result = ArrayValue::get($arr, $keys);
        $this->assertEquals('new', $result);

        // 2 dimensions
        $arr = $this->getTestArray();
        $keys = explode('.', 'key2.sub1');
        $result = ArrayValue::set($arr, $keys, 'new');
        $expected = $arr;
        $expected['key2']['sub1'] = 'new';
        $this->assertEquals($expected, $result);

        $result = ArrayValue::get($arr, $keys);
        $this->assertEquals('new', $result);

        // With undefined key
        $keys = explode('.', 'not.exsting.key');
        $result = ArrayValue::set($arr, $keys, 'new');
        $expected = $arr;
        $expected['not']['exsting']['key'] = 'new';
        $this->assertEquals($expected, $result);
    }

    /**
     * Test set function
     *
     * @covers ::read
     */
    public function testRead()
    {
        $arr = $this->getTestArray();

        $result = ArrayValue::read($arr, 'key');
        $this->assertEquals('value', $result);

        $result = ArrayValue::read($arr, 'notexistingkey', 'test');
        $this->assertEquals('test', $result);
    }
}
