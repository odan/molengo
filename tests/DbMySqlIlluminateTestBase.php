<?php

namespace Molengo\Test;

use Molengo\Db\DbMySqlIlluminate;

/**
 * @coversDefaultClass \Molengo\Db\DbMySqlIlluminate
 */
class DbMySqlIlluminateTestBase extends DbMySqlTestBase
{

    protected static $db;

    /**
     * Empty test
     *
     * @return void
     */
    public function testEmpty()
    {

    }

    /**
     * Returns database object
     *
     * @return \Molengo\Db\DbMySqlIlluminate
     */
    protected static function getDb()
    {
        if (static::$db === null) {
            static::$db = DbMySqlIlluminate::connect(static::$dbConfig);
            static::$db->setLogger(static::getLogger());
        }
        return static::$db;
    }

    /**
     * Create test table
     *
     * @return int
     */
    protected function createTestTable()
    {
        $db = static::getDb();
        $conn = $db->getConnection();
        $schema = $conn->getSchemaBuilder();

        $schema->dropIfExists('test');
        $schema->dropIfExists('temp');
        $schema->dropIfExists('test_copy');

        // MEMORY is faster then InnoDB
        $result = $conn->getPdo()->exec("CREATE TABLE `test` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `keyname` varchar(255) COLLATE utf8_unicode_ci,
            `keyvalue` varchar(255) COLLATE utf8_unicode_ci,
            `boolvalue` tinyint(1) NOT NULL,
            `created` DATETIME DEFAULT NULL,
            `created_user_id` INT(11) DEFAULT NULL,
            `updated` DATETIME DEFAULT NULL,
            `updated_user_id` INT(11) DEFAULT NULL,
            `deleted` DATETIME DEFAULT NULL,
            `deleted_user_id` INT(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `created_user_id` (`created_user_id`),
            KEY `updated_user_id` (`updated_user_id`),
            KEY `deleted_user_id` (`deleted_user_id`)
            ) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

        return $result;
    }
}
