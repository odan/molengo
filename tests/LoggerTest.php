<?php

namespace Molengo\Test;

class LoggerTest extends \Molengo\Test\TestCase
{

    public function testMonolog()
    {
        // create a log channel
        $log = new \Monolog\Logger('test');

        //$formatter = new \Monolog\Formatter\LineFormatter(
        //        "[%datetime%] %channel%.%level_name%: %message%\n"
        //);

        $logDir = realpath(__DIR__ . '/../build/test/logs');
        $stream = $logDir . '/log.txt';
        // filename, maxFiles = 0, $level = Logger::DEBUG, bubble = true, filePermission
        $handler = new \Monolog\Handler\RotatingFileHandler(
            $stream, 0, \Monolog\Logger::DEBUG, true, 0775
        );
        // handler->setFormatter(formatter);

        $log->pushHandler($handler);
        // log->pushHandler(new StreamHandler(stream, Logger::WARNING));
        // Add records to the log
        $log->addWarning('Foo');
        $log->addError('Bar');
        $log->addDebug('debug');
    }
}
