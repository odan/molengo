# Molengo

Molengo, a PHP micro framework

![molengo-logo](http://www.molengo.com/assets/img/logo.png)

## Shields

[![Latest Version](https://img.shields.io/github/release/odan/molengo.svg?style=flat-square)](https://github.com/loadsys/odan/molengo/releases)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Crutinizer](https://img.shields.io/scrutinizer/g/odan/molengo.svg)](https://scrutinizer-ci.com/g/odan/molengo)
[![Code Climate](https://img.shields.io/codeclimate/github/odan/molengo.svg)](https://codeclimate.com/github/odan/molengo)
[![Build Status](https://travis-ci.org/odan/molengo.svg?branch=master&style=flat-square)](https://travis-ci.org/odan/molengo)
[![Coverage Status](https://coveralls.io/repos/odan/molengo/badge.svg)](https://coveralls.io/r/odan/molengo)
[![Total Downloads](https://img.shields.io/packagist/dt/odan/molengo.svg?style=flat-square)](https://packagist.org/packages/odan/molengo)
[![Repo Size](https://reposs.herokuapp.com/?path=odan/molengo&style=flat)](https://reposs.herokuapp.com/?path=odan/molengo)

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/49871c79-ddce-4285-a8ec-a691ae377705/big.png)](https://insight.sensiolabs.com/projects/49871c79-ddce-4285-a8ec-a691ae377705)

## Requirements ##

* PHP 5.5+

## Installation

### Composer

````bash
$ composer require odan/molengo
````

### composer.json

```javascript
{
    "require": {
        "odan/molengo": "1.2.*"
    }
}
```
Run:
````bash
$ composer install
````

## Documentation

* [read more](https://molengo.readme.io/v1.1/docs)


## Website

<http://www.molengo.com/>

## Twitter

Follow [@molengophp](http://www.twitter.com/molengophp) on Twitter to receive news and updates about the framework.

## License

The Molengo Framework is released under the MIT public license.

<https://github.com/odan/molengo/edit/master/LICENSE.md>
