<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Auth;

use Molengo\Db\DbMySqlIlluminate;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * The current user session (authenticated or otherwise).
 */
class UserSession
{

    /** @var DbMySqlIlluminate */
    protected $db;

    /** @var SessionInterface */
    protected $session;

    /**
     * Secret session key
     *
     * @var string
     */
    protected $secret = '';

    /**
     * Constructor
     *
     * @param DbMySqlIlluminate $db
     * @param SessionInterface $session
     */
    public function __construct(DbMySqlIlluminate $db, SessionInterface $session)
    {
        $this->db = $db;
        $this->session = $session;
    }

    /**
     * Set secret session key
     *
     * @param string $secret secret session key
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * Returns user by username and password
     *
     * @param string $username
     * @param string $password
     * @return array|null
     */
    public function getUserByLogin($username, $password)
    {
        $row = $this->db->table('user')
            ->where('username', $username)
            ->where('disabled', 0)
            ->first();

        if (empty($row) || !$this->verifyHash($password, $row['password'])) {
            $row = null;
        }
        return $row;
    }

    /**
     * Returns user by user id
     *
     * @param int $id
     * @return array
     */
    public function getUserById($id)
    {
        return $this->db->table('user')->where('id', $id)->first();
    }

    /**
     * Login user with username and password
     *
     * @param array $params
     * @return bool
     */
    public function login($params = array())
    {
        $username = $params['username'];
        $password = $params['password'];

        // Check username and password
        $user = $this->getUserByLogin($username, $password);

        if (empty($user)) {
            return false;
        }

        // Login ok
        $result = true;

        // Create new session id
        $this->session->invalidate();

        // Store user settings in session
        $this->set('user.id', $user['id']);
        $this->set('user.role', $user['role']);
        $this->setLocale($user['locale']);
        return $result;
    }

    /**
     * Logout user session
     *
     * @return void
     */
    public function logout()
    {
        $this->set('user.id', null);
        $this->set('user.role', null);
        $this->set('user.locale', null);

        // Clears all session data and regenerates session ID
        $this->session->invalidate();
    }

    /**
     * Returns secure password hash
     *
     * @param string $password
     * @param int $algo
     * @param array $options
     * @return string
     */
    public function createHash($password, $algo = 1, $options = array())
    {
        if (function_exists('password_hash')) {
            // php >= 5.5
            $hash = password_hash($password, $algo, $options);
        } else {
            $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
            $salt = base64_encode($salt);
            $salt = str_replace('+', '.', $salt);
            $hash = crypt($password, '$2y$10$' . $salt . '$');
        }
        return $hash;
    }

    /**
     * Returns true if password and hash is valid
     *
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function verifyHash($password, $hash)
    {
        if (function_exists('password_verify')) {
            // php >= 5.5
            $result = password_verify($password, $hash);
        } else {
            $hash2 = crypt($password, $hash);
            $result = $hash == $hash2;
        }
        return $result;
    }

    /**
     * Check if token is correct for this string
     *
     * @param string $value
     * @param string $token
     * @return boolean
     */
    public function checkToken($value, $token)
    {
        $realHash = $this->getToken($value);
        $result = ($token === $realHash);
        return $result;
    }

    /**
     * Generate Hash-Token from string
     *
     * @param string $value
     * @param string $secret
     * @return string
     */
    public function getToken($value, $secret = null)
    {
        if ($secret === null) {
            $secret = $this->secret;
        }
        // Create real key for value
        $sessionId = $this->session->getId();
        $realHash = sha1($value . $sessionId . $secret);
        return $realHash;
    }

    /**
     * Set user info
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->session->set($key, $value);
    }

    /**
     * Get current user information
     *
     * @param string $key
     * @return mixed
     */
    public function get($key, $default = '')
    {
        $mixReturn = $this->session->get($key, $default);
        return $mixReturn;
    }

    /**
     * Check user permission
     *
     * @param string|array $role (e.g. 'ROLE_ADMIN' or 'ROLE_USER')
     * or array('ROLE_ADMIN', 'ROLE_USER')
     * @return boolean
     */
    public function is($role)
    {
        // Current user role
        $userRole = $this->get('user.role');

        // Full access for admin
        if ($userRole === 'ROLE_ADMIN') {
            return true;
        }
        if ($role === $userRole) {
            return true;
        }
        if (is_array($role) && in_array($userRole, $role)) {
            return true;
        }
        return false;
    }

    /**
     * Check if user is authenticated (logged in)
     *
     * @return boolean
     */
    public function isValid()
    {
        $id = $this->get('user.id');
        $status = !empty($id);
        return $status;
    }

    /**
     * Change user session locale
     *
     * @param string $locale e.g. de_DE
     * @param string $domain default = null (messages)
     * @return bool
     */
    public function setLocale($locale, $domain = null)
    {
        $this->set('user.locale', $locale);
        $this->set('user.domain', $domain);
        return true;
    }
}
