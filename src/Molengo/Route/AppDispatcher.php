<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Route;

use Molengo\Config\Container\AppInterface;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\Response;

/**
 * AppDispatcher
 */
class AppDispatcher
{

    /**
     * AppInterface
     *
     * @var AppInterface
     */
    protected $app;

    /**
     * Constructor
     *
     * @param AppInterface $app
     */
    public function __construct(AppInterface $app)
    {
        $this->app = $app;
    }

    /**
     * Call controller action
     *
     * @param string $controller
     * @param string $action
     * @return Response
     */
    public function dispatch($controller = null, $action = null)
    {
        if (!isset($controller)) {
            $controller = 'index';
        }
        if (!isset($action)) {
            $action = 'index';
        }

        $controllerClass = $this->getControllerClass($controller);
        $reflection = new ReflectionMethod($controllerClass, $action);

        if (!$reflection->isPublic()) {
            return new Response('Forbidden', 403);
        }

        $obj = new $controllerClass($this->app);
        $response = $obj->{$action}();
        if ($response instanceof Response) {
            return $response;
        }
        return $this->app->response();
    }

    /**
     * Return controller class name
     *
     * @param string $controller
     * @return string
     */
    protected function getControllerClass($controller)
    {
        $result = str_replace(array('_', '-'), ' ', strtolower($controller));
        $result = ucwords(strtolower($result));
        $result = str_replace(' ', '', $result);
        $result = '\\App\\Controller\\' . $result . 'Controller';
        return $result;
    }
}
