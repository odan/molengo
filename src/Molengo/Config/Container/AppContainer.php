<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Config\Container;

use League\Plates\Engine;
use League\Route\RouteCollection;
use Molengo\Auth\UserSession;
use Molengo\Config\Container\AppInterface;
use Molengo\Db\DbMySqlIlluminate;
use Molengo\Error\ErrorHandler;
use Molengo\Http\Http;
use Molengo\Mail\MessageInterface;
use Molengo\Mail\MessagePhpMailer;
use Molengo\Map\ArrayValue;
use Molengo\Route\AppDispatcher;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\MoFileLoader;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

/**
 * Application (Dependency Injection Container)
 *
 * With a Dependency Injection Container you only create your objects in one
 * place, so you only ever need to update them in one place.
 *
 * The difference between a Service Locator and a Dependency Injection Container
 * is how you consume them. The implementation of both can be identical,
 * but with a Service Locator you inject the container and ask it for the
 * object you want, whereas with a Dependency Injection Container you use
 * it to construct objects, but a Dependency Injection Container should only
 * ever call itself, and never be called by any other objects.
 */
class AppContainer implements AppInterface
{

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = array();

    /**
     * Database
     *
     * @var DbMySqlIlluminate
     */
    protected $db = null;

    /**
     * View template
     *
     * @var Engine
     */
    protected $view = null;

    /**
     * Router and dispatcher
     *
     * @var RouteCollection
     */
    protected $router = null;

    /**
     * Session
     *
     * @var SessionInterface
     */
    protected $session = null;

    /**
     * Request
     *
     * @var Request
     */
    protected $request = null;

    /**
     * Response
     *
     * @var Response
     */
    protected $response = null;

    /**
     * Translation
     *
     * @var Translator
     */
    protected $translator = null;

    /**
     * User
     *
     * @var UserSession
     */
    protected $user = null;

    /**
     * Static instance ob App
     *
     * @var AppInterface
     */
    protected static $instance;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $log = null;

    /**
     * Error handler
     *
     * @var ErrorHandler
     */
    protected $error = null;

    /**
     * Returns application static instance
     *
     * @return static
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Main
     *
     * @return void
     */
    public static function main()
    {
        $app = static::getInstance();
        $app->init();
        $app->run();
        $app->close();
    }

    /**
     * Set config
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value)
    {
        $keys = explode('.', $key);
        ArrayValue::set($this->config, $keys, $value);
    }

    /**
     * Get config
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $keys = explode('.', $key);
        return ArrayValue::get($this->config, $keys, $default);
    }

    /**
     * Init configuration
     *
     * @return static
     */
    protected function config()
    {
        $envs = array('Bootstrap', 'Local');
        foreach ($envs as $env) {
            $configClass = '\\App\\Config\\Environment\\' . $env;
            if (class_exists($configClass)) {
                $instance = new $configClass();
                $instance->load($this->getInstance());
            }
        }
        return static::getInstance();
    }

    /**
     * Init all objects
     *
     * @return void
     */
    public function init()
    {
        // Load config
        $this->config();

        // Error reporting and handler
        $this->initErrorHandling();

        // Logger
        $this->initLogger();

        // Session
        $this->initSession();

        // User session
        $this->initUser();

        // Url mapping
        $this->initRouter();

        // View templates
        $this->initView();
    }

    /**
     * Start application
     *
     * @return void
     */
    public function run()
    {
        // Dispatching
        $dispatcher = $this->router()->getDispatcher();
        $request = $this->request();
        $method = $request->getMethod();
        $http = new Http($request, $this->response());
        $uri = $http->getRealBasePath();
        $response = $dispatcher->dispatch($method, $uri);
        $response->send();
    }

    /**
     * Close app
     *
     * @return void
     */
    public function close()
    {

    }

    /**
     * Session object
     *
     * @return SessionInterface
     */
    public function session()
    {
        if ($this->session === null) {
            $this->initSession();
        }
        return $this->session;
    }

    /**
     * Init session
     *
     * @return void
     */
    protected function initSession()
    {
        $options = $this->get('session');
        $storage = new NativeSessionStorage($options, new NativeFileSessionHandler());
        $this->session = new Session($storage);
        $this->session->start();
    }

    /**
     * User object
     *
     * @return UserSession
     */
    public function user()
    {
        if ($this->user === null) {
            $this->initUser();
        }
        return $this->user;
    }

    /**
     * Init user session
     *
     * @return void
     */
    protected function initUser()
    {
        $this->user = new UserSession($this->db(), $this->session());
        $this->user->setSecret($this->get('app.secret'));

        // Init language
        $locale = $this->session()->get('user.locale');
        if (empty($locale)) {
            $locale = 'en_US';
        }
        $this->user->setLocale($locale);
    }

    /**
     * Translation object
     *
     * @return Translator
     */
    public function translator()
    {
        if ($this->translator === null) {
            $this->initTranslator();
        }
        return $this->translator;
    }

    /**
     * Init translation
     *
     * @return void
     */
    protected function initTranslator()
    {
        // Default locale
        $locale = $this->session()->get('user.locale', 'en_US');
        $domain = $this->session()->get('user.domain', 'messages');
        $domain = empty($domain) ? 'messages' : $domain;

        $this->translator = new Translator($locale, new MessageSelector());
        $this->translator->addLoader('mo', new MoFileLoader());
        $moFile = $this->get('path.locale') . "/$locale/LC_MESSAGES/$domain.mo";
        $this->translator->addResource('mo', $moFile, $locale, $domain);
    }

    /**
     * Template object
     *
     * @return Engine
     */
    public function view()
    {
        if ($this->view === null) {
            $this->initView();
        }
        return $this->view;
    }

    /**
     * Init view template
     *
     * @return void
     */
    protected function initView()
    {
        $this->view = new Engine($this->get('path.view'), null);

        // Add folder shortcut (assets::file.js)
        $this->view->addFolder('assets', $this->get('path.assets'));

        $this->initViewCache();
    }

    /**
     * Init view assets cache
     *
     * @return void
     */
    protected function initViewCache()
    {
        $http = new Http($this->request(), $this->response());

        // Register Asset extension
        $options = array(
            // View base path
            'cachepath' => $this->get('path.view_cache'),
            // Create different hash for each language
            'cachekey' => $this->session()->get('locale'),
            // Base Url for public cache directory
            'baseurl' => $http->getRealBaseUrl('/'),
            // JavaScript and CSS compression
            'minify' => $this->get('view.minify')
        );
        $this->view->loadExtension(new \Odan\Plates\Extension\AssetCache($options));
    }

    /**
     * Router object
     *
     * @return RouteCollection
     */
    public function router()
    {
        if ($this->router === null) {
            $this->initRouter();
        }
        return $this->router;
    }

    /**
     * Init Router and define url mappings
     *
     * @return void
     */
    protected function initRouter()
    {
        $this->router = new RouteCollection();
        $strategy = new \League\Route\Strategy\UriStrategy();
        $this->router->setStrategy($strategy);

        // Default rule
        $this->router->addRoute(array('GET', 'POST'), '/', function () {
            $dispatcher = new AppDispatcher($this);
            return $dispatcher->dispatch();
        });
        // Controller rules
        $this->router->addRoute(array('GET', 'POST'), '/{controller}', function ($controller) {
            $dispatcher = new AppDispatcher($this);
            return $dispatcher->dispatch($controller);
        });
        $this->router->addRoute(array('GET', 'POST'), '/{controller}/{action}', function ($controller, $action) {
            $dispatcher = new AppDispatcher($this);
            return $dispatcher->dispatch($controller, $action);
        });
    }

    /**
     * Return request object
     *
     * @return Request
     */
    public function request()
    {
        if ($this->request === null) {
            $this->request = Request::createFromGlobals();
        }
        return $this->request;
    }

    /**
     * Response object
     *
     * @return Response
     */
    public function response()
    {
        if ($this->response === null) {
            $this->response = new Response();
        }
        return $this->response;
    }

    /**
     * Create E-Mail object
     *
     * @return MessageInterface
     */
    public function mail()
    {
        $email = new MessagePhpMailer();
        $email->setConfig($this->get('smtp'));
        return $email;
    }

    /**
     * Database object
     *
     * @return DbMySqlIlluminate
     */
    public function db()
    {
        if ($this->db === null && $config = $this->get('db')) {
            // Open database connection
            $this->db = DbMySqlIlluminate::connect($config);

            // Inject PSR logger
            if ($this->log) {
                $this->db->setLogger($this->log);
            }
        }
        return $this->db;
    }

    /**
     * Returns logger
     *
     * @return LoggerInterface Logger
     */
    public function logger()
    {
        if ($this->log === null) {
            $this->initLogger();
        }
        return $this->log;
    }

    /**
     * Init logger
     *
     * @return void
     */
    protected function initLogger()
    {
        // Create a log channel
        $this->log = new Logger('app');
        $level = $this->get('log.level', \Monolog\Logger::ERROR);
        $logDir = $this->get('path.log');
        $logFile = $logDir . '/log.txt';
        $handler = new RotatingFileHandler(
            $logFile, 0, $level, true, 0775
        );
        $this->log->pushHandler($handler);
    }

    /**
     * Init error handler
     *
     * @return void
     */
    protected function initErrorHandling()
    {
        $this->error = new ErrorHandler($this->response(), $this->logger());
        set_error_handler(array($this->error, 'handleError'), error_reporting());
        set_exception_handler(array($this->error, 'handleException'));
        register_shutdown_function(array($this->error, 'handleShutdown'));
    }
}
