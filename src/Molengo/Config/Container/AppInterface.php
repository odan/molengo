<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Config\Container;

use League\Plates\Engine;
use League\Route\RouteCollection;
use Molengo\Auth\UserSession;
use Molengo\Db\DbMySqlIlluminate;
use Molengo\Mail\MessageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Application (Service Locator)
 */
interface AppInterface
{

    /**
     * Returns application static instance
     *
     * @return static
     */
    public static function getInstance();

    /**
     * Main
     *
     * @return void
     */
    public static function main();

    /**
     * Init all objects
     *
     * @return void
     */
    public function init();

    /**
     * Start application
     *
     * @return void
     */
    public function run();

    /**
     * Close application
     *
     * @return void
     */
    public function close();

    /**
     * Session object
     *
     * @return SessionInterface
     */
    public function session();

    /**
     * User object
     *
     * @return UserSession
     */
    public function user();

    /**
     * Translation object
     *
     * @return Translator
     */
    public function translator();

    /**
     * Html template object
     *
     * @return Engine
     */
    public function view();

    /**
     * Rrouter object
     *
     * @return RouteCollection
     */
    public function router();

    /**
     * Request object
     *
     * @return Request
     */
    public function request();

    /**
     * Response object
     *
     * @return Response
     */
    public function response();

    /**
     * Create E-Mail object
     *
     * @return MessageInterface
     */
    public function mail();

    /**
     * Database object
     *
     * @return DbMySqlIlluminate
     */
    public function db();

    /**
     * Logger
     *
     * @return LoggerInterface Logger
     */
    public function logger();

    /**
     * Set config
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value);

    /**
     * Get config
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key, $default = null);
}
