<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Config\Environment;

use Molengo\Config\Container\AppInterface;

/**
 * Environment config loader
 */
class Loader
{

    /**
     * Load application environment configuration with
     * from first existing file.
     *
     * @param AppInterface $app
     * @param array $fileNames path and filename to env.php
     * @return bool
     */
    public function load($app, $fileNames)
    {
        foreach ($fileNames as $fileName) {
            if (file_exists($fileName)) {
                return $this->loadFile($app, $fileName);
            }
        }
    }

    /**
     * Load application environment configuration
     *
     * @param AppInterface $app
     * @param string $fileName path and filename to env.php
     * @return bool
     */
    public function loadFile(AppInterface $app, $fileName)
    {
        // Load environment class
        $config = require $fileName;
        if (isset($config['env.class'])) {
            $className = $config['env.class'];
            if (class_exists($className)) {
                $instance = new $className();
                $instance->load($app);
            }
        }

        // Load sensitive informations from environment file into app
        foreach ($config as $key => $value) {
            $app->set($key, $value);
        }
        return true;
    }
}
