<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Util;

/**
 * String utils
 */
class Str
{

    /**
     * Returns a truncated string
     *
     * @param string $str
     * @param int $length
     * @param string $append
     * @return string
     */
    public static function truncate($str, $length = 255, $append = '')
    {
        $textlen = strlen($str);
        if ($textlen > $length) {
            $appendlen = strlen($append);
            $str = substr($str, 0, $textlen - $appendlen) . $append;
        }
        return $str;
    }

    /**
     * Returns a random string
     *
     * @param int $length Password Length: (4 - 64 chars)
     * @param bool $lowercase  Include Letters: (e.g. abcdef)
     * @param bool $uppercase Include Mixed Case: (e.g. AbcDEf)
     * @param bool $numbers  Include Numbers: (e.g. a9b8c7d)
     * @param bool $punctuation Include Punctuation: (e.g. a!b*c_d)
     * @return string
     */
    public static function random($length, $lowercase = true, $uppercase = true, $numbers = true, $punctuation = false)
    {
        $result = '';
        $charset = '';

        if ($lowercase === true) {
            $charset .= "abcdefghijklmnopqrstuvwxyz";
        }
        if ($uppercase === true) {
            $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        if ($numbers === true) {
            $charset .= "1234567890";
        }
        if ($punctuation === true) {
            $charset .= "!$%&/()=?+#'-_:@{}*.<>\"";
        }

        $charsetlen = strlen($charset);
        for ($i = 0; $i < $length; $i++) {
            $pos = mt_rand(0, $charsetlen - 1);
            $result .= substr($charset, $pos, 1);
        }
        return $result;
    }

    /**
     * Returns a random UUID
     *
     * @return string
     */
    public static function uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Returns a UTF-8 encoded string or array
     *
     * @param mixed $value
     * @return mixed
     */
    public static function encodeUtf8($value)
    {
        return static::encodeCharset($value, 'UTF-8', 'UTF-8', false);
    }

    /**
     * Returns a ISO-8859-1 encoded string or array
     *
     * @param mixed $value
     * @return mixed
     */
    public static function encodeIso($value)
    {
        return static::encodeCharset($value, 'UTF-8', 'ISO-8859-1', true);
    }

    /**
     * Returns a encoded string or array
     *
     * @param mixed $value
     * @param string $fromEncoding
     * @param string $toEncoding
     * @param bool $comparison
     * @return mixed
     */
    protected static function encodeCharset($value, $fromEncoding, $toEncoding, $comparison)
    {
        if ($value === null || $value === '') {
            return $value;
        }
        if (is_array($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = static::encodeCharset($val, $toEncoding, $toEncoding, $comparison);
            }
            return $value;
        } else {
            if (mb_check_encoding($value, $fromEncoding) === $comparison) {
                return mb_convert_encoding($value, $toEncoding);
            } else {
                return $value;
            }
        }
    }

    /**
     * Returns json string from value
     *
     * @param mixed $value
     * @param int $options
     * @return string
     */
    public static function encodeJson($value, $options = 0)
    {
        return json_encode(static::encodeUtf8($value), $options);
    }

    /**
     * Returns array from json string
     *
     * @param string $json
     * @return array
     */
    public static function decodeJson($json)
    {
        return json_decode($json, true);
    }

    /**
     * Takes a string as input and creates a human-friendly URL string.
     * This is useful if, for example, you have a blog in which you'd like to
     * use the title of your entries in the URL. Example:
     *
     * $title = "What's wrong with CSS?";
     * $url_title = url_title($title);
     * Produces: Whats-wrong-with-CSS
     *
     * @param string $str
     * @return string
     */
    public static function urlTitle($str)
    {
        if ($str === null || $str === '') {
            return '';
        }

        $normal = static::charMapUtf8Array();

        // replace all silly chars
        $str = preg_replace_callback('/[^a-z0-9A-Z]/u', function($match) use ($normal) {
            $code = static::uord($match[0]);
            if (isset($normal[$code])) {
                return $normal[$code];
            }
            return '-';
        }, $str);

        // Replace 2 or more contiguous occurrences
        // of any minus character with a single minus
        $str = trim(preg_replace('/\-{2,}/', '-', $str), '-');
        return $str;
    }

    /**
     * Returns a mapping array (from utf-8 charcode to normal char)
     * @return array
     */
    protected static function charMapUtf8Array()
    {
        return array(
            228 => 'a',
            246 => 'o',
            252 => 'u',
            196 => 'A',
            214 => 'O',
            220 => 'U',
            223 => 'sz',
            233 => 'e',
            232 => 'e',
            234 => 'e',
            235 => 'e',
            231 => 'c',
            219 => 'U',
            338 => 'O',
            339 => 'O',
            206 => 'I',
            202 => 'E',
            200 => 'E',
            201 => 'E',
            233 => 'e',
            199 => 'C',
            231 => 'c',
            194 => 'A',
            226 => 'a',
            192 => 'A',
            224 => 'a'
        );
    }

    /**
     * Return a specific character (utf-8)
     * @param int $code
     * @return string
     */
    public static function uchr($code)
    {
        return mb_convert_encoding(pack('n', $code), 'UTF-8', 'UTF-16BE');
    }

    /**
     * Return an UTF-8 value of character
     * @param string $char
     * @return num
     */
    public static function uord($char)
    {
        $k = mb_convert_encoding($char, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));
        return $k2 * 256 + $k1;
    }

    /**
     * Validate E-Mail address
     *
     * @param string $email
     * @return bool
     */
    public static function isEmail($email = null)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Returns a trimmed array
     *
     * @param array $array
     * @return array
     */
    public static function trim($array)
    {
        if (is_array($array)) {
            foreach ($array as $key => $val) {
                $array[$key] = static::trim($val);
            }
            return $array;
        } else {
            return trim($array);
        }
    }
}
