<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

/**
 * Convert all applicable characters to HTML entities
 *
 * @param string $text
 * @return string
 */
function gh($text)
{
    // Skip empty strings
    if ($text === null || $text === '') {
        return '';
    }

    // Convert to utf-8
    if (!mb_check_encoding($text, 'UTF-8')) {
        $text = mb_convert_encoding($text, 'UTF-8');
    }

    $text = htmlentities($text, ENT_QUOTES, "UTF-8");

    // Convert non printable and non ascii chars to numeric entity
    // This will match a single non-ASCII character
    // This is a valid PCRE (Perl-Compatible Regular Expression).
    $text = preg_replace_callback('/[^\x20-\x7E]/u', function($match) {
        return mb_encode_numericentity($match[0], array(0x0, 0xffff, 0, 0xffff), 'UTF-8');
    }, $text);

    return $text;
}

/**
 * Returns HTML encoded string. Newlines are converted to HTML <br> tag.
 *
 * @param string $str
 * @return string
 */
function ghbr($str)
{
    if ($str === null || $str === '') {
        return '';
    }
    $result = '';
    $arr = explode("\n", $str);
    if (!empty($arr) && is_array($arr)) {
        foreach ($arr as $key => $row) {
            $arr[$key] = gh($row);
        }
        $result = implode('<br>', $arr);
    }
    return $result;
}

/**
 * Print Html and nl2br encoded string
 *
 * @param string $str
 */
function whbr($str)
{
    echo ghbr($str);
}

/**
 * Write html encoded string
 *
 * @param string $str
 */
function wh($str)
{
    echo gh($str);
}

/**
 * URL Encoding: Write URL encoded string
 *
 * @param string $str
 */
function wu($str)
{
    echo urlencode($str);
}

/**
 * URL Encoding
 *
 * @param string $str
 */
function gu($str)
{
    return urlencode($str);
}

/**
 * HTML Attribute Encoding
 *
 * @param string $str string to encode
 */
function ga($str)
{
    return htmlspecialchars($str);
}

/**
 * HTML Attribute Encoding: Write attribute encoded string
 *
 * @param string $str string to encode and print
 */
function wa($str)
{
    echo htmlspecialchars($str);
}

/**
 * Return Array element value (get value)
 *
 * @param array $arr
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function gv(&$arr, $key, $default = null)
{
    return \Molengo\Map\ArrayValue::read($arr, $key, $default);
}

/**
 * Returns true if the variable is blank.
 * When you need to accept these as valid, non-empty values:
 *
 * - 0 (0 as an integer)
 * - 0.0 (0 as a float)
 * - "0" (0 as a string)
 *
 * @param mixed $value
 * @return boolean
 */
function blank($value)
{
    return empty($value) && !is_numeric($value);
}

/**
 * PSR-3: Interpolates context values into the message placeholders.
 *
 * The message MAY contain placeholders which implementors MAY replace
 * with values from the context array.
 *
 * Placeholder names MUST correspond to keys in the context array.
 *
 * Placeholder names MUST be delimited with
 * a single opening brace { and a single closing brace }.
 *
 * There MUST NOT be any whitespace between the delimiters
 * and the placeholder name.
 *
 * Placeholder names SHOULD be composed only of the characters A-Z, a-z, 0-9,
 * underscore _, and period .. The use of other characters is reserved for
 * future modifications of the placeholders specification.
 *
 * Details:
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 *
 * @example
 *
 * // a message with brace-delimited placeholder names
 * $message = "User {username} created";
 *
 * // a context array of placeholder names => replacement values
 * $context = array('username' => 'bolivar');
 *
 * // echoes "Username bolivar created"
 * echo interpolate($message, $context);
 *
 * @param string $message
 * @param array $context
 * @return string
 */
function interpolate($message, array $context = array())
{
    // build a replacement array with braces around the context keys
    $replace = array();
    foreach ($context as $key => $val) {
        $replace['{' . $key . '}'] = $val;
    }

    // interpolate replacement values into the message and return
    return strtr($message, $replace);
}

/**
 * Shortcut for interpolate
 *
 * @param string $message
 * @param array $context
 * @return string
 */
function i($message, array $context = array())
{
    return interpolate($message, $context);
}

/**
 * Returns string by error type
 *
 * @param int $type error code
 * @return string
 */
function error_type_text($type)
{
    $map = array(
        E_ERROR => 'E_ERROR',
        E_WARNING => 'E_WARNING',
        E_PARSE => 'E_PARSE',
        E_NOTICE => 'E_NOTICE',
        E_CORE_ERROR => 'E_CORE_ERROR',
        E_CORE_WARNING => 'E_CORE_WARNING',
        E_COMPILE_ERROR => 'E_COMPILE_ERROR',
        E_CORE_WARNING => 'E_CORE_WARNING',
        E_USER_ERROR => 'E_USER_ERROR',
        E_USER_WARNING => 'E_USER_WARNING',
        E_USER_NOTICE => 'E_USER_NOTICE',
        E_STRICT => 'E_STRICT',
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
        E_DEPRECATED => 'E_DEPRECATED',
        E_USER_DEPRECATED => 'E_USER_DEPRECATED'
    );
    return isset($map[$type]) ? $map[$type] : null;
}

/**
 * Text translation (I18n)
 *
 * @param string $message
 * @param array $context
 * @return string
 *
 * <code>
 * echo __('Hello');
 * echo __('There are {number} persons logged', array('number' => 7));
 * </code>
 */
function __($message, array $context = array())
{
    static $translator = null;
    if ($translator === null) {
        $translator = \App\Config\Container\Application::getInstance()->translator();
    }
    $message = $translator->trans($message);
    if (!empty($context)) {
        $message = interpolate($message, $context);
    }
    return $message;
}
