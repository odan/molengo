<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\DateTime;

use DateTime;
use Exception;

/**
 * Date and time helper
 */
class Time
{

    /**
     * Returns the current date and time in IS0-8601 format
     * Format: Y-m-d H:i:s
     *
     * @return string
     */
    public static function now()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * Converts any date/time format.
     * Support for dates <= 1901.
     *
     * @param string $time
     * @param string $format (default is d.m.Y)
     * @param mixed $default
     * @return string or $mixDefault (null)
     *
     * <code>
     * echo Time::format('2011-03-28 15:14:30'); // '28.03.1982'
     * echo Time::format('2011-03-28 15:10:5', 'd.m.Y H:i:s'); // '28.03.1982 15:10:05'
     * echo Time::format('1900-3-22 23:01:45', 'H:i:s'); // '23:01:45'
     * echo Time::format('2014-14-31', 'H:i:s', 'not valid'); // 'not valid'
     * </code>
     */
    public static function format($time, $format = 'd.m.Y', $default = null)
    {
        $result = $default;
        if (empty($time) || $time === '0000-00-00 00:00:00' ||
            $time === '0000-00-00') {
            return $default;
        }
        try {
            $date = new DateTime($time);
            $result = $date->format($format);
            if ($result === false) {
                return $default;
            }
        } catch (Exception $ex) {
            return $default;
        }
        return $result;
    }

    /**
     * Validate date format via ereg. Delimeter can be a . (point)
     * Returns true if the date given is valid; otherwise returns false.
     *
     * <code>
     * $valid = Time::isDate('28.03.1982');
     * </code>
     *
     * @param string $date format: dd.mm.yyyy  e.g. 30.12.2002
     * @param int $minYear mininum year
     * @param int $maxYear maximum year
     * @return boolean
     */
    public static function isDate($date, $minYear = 1, $maxYear = 32767)
    {
        $regex = "/^([0-9]{2})[.]([0-9]{2})[.]([0-9]{4})$/";
        $matches = array();
        if (!preg_match($regex, $date, $matches)) {
            return false;
        }
        if ($matches[3] >= $minYear && $matches[3] <= $maxYear) {
            if (checkdate($matches[2], $matches[1], $matches[3])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate time format
     *
     * <code>
     * $valid = Time::isTime('15:10:00');
     * </code>
     *
     * @param string $time format: hh:mm:ss
     * @return bool
     */
    public static function isTime($time)
    {
        $result = false;
        if (preg_match("/^([0-9]{2})[:]([0-9]{2})[:]([0-9]{2})$/", $time)) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', '1970-01-01 ' . $time);
            $result = ($date !== false);
        }
        return $result;
    }
}
