<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Db;

use Illuminate\Database\Query\Builder;

/**
 * DbMySqlIlluminateQueryBuilder
 */
class DbMySqlIlluminateQueryBuilder extends Builder
{

    /**
     * Run the query as a "select" statement against the connection.
     *
     * @return array
     */
    protected function runSelect()
    {
        // Before query
        $status = $this->getConnection()->getEventDispatcher()->fire('query.beforeSelect', $this);

        // Check result from callback
        $result = null;
        if ($status !== false) {
            $sql = $this->toSql();
            $result = $this->connection->select($sql, $this->getBindings(), !$this->useWritePdo);

            // After query
            $this->getConnection()->getEventDispatcher()->fire('query.afterSelect', $this);
        }
        return $result;
    }
}
