<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Db;

use Illuminate\Database\Capsule\Manager;
use Molengo\Db\DbMySqlGeneralLog;
use Molengo\Db\DbMySqlIlluminateDispatcher;
use Molengo\Db\DbMySqlIlluminateQueryBuilder;
use PDO;
use Psr\Log\LoggerInterface;

/**
 * Database Management for MySql
 */
class DbMySqlIlluminateManager extends Manager
{

    /** @var LoggerInterface */
    protected $logger = null;

    /**
     * Open db connection with DSN and returns connection
     *
     * @param array $config configuration
     * @return static
     */
    public static function connect($config)
    {
        $db = new static();
        $db->addConnection($config);

        // Make this Capsule instance available globally via static methods
        $db->setAsGlobal();

        $connection = $db->getConnection();

        // Set default to array (instead of stdObject)
        $connection->setFetchMode(PDO::FETCH_ASSOC);

        // Convert column names to lower case.
        $pdo = $connection->getPdo();
        $pdo->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);

        // Enable UTF-8 encoding
        $pdo->exec('SET NAMES utf8;');
        return $db;
    }

    /**
     * Get a fluent query builder instance.
     *
     * @param  string  $table
     * @param  string  $connection
     * @return DbMySqlIlluminateQueryBuilder
     */
    public static function table($table, $connection = null)
    {
        $builder = static::newBaseQueryBuilder($connection);
        $builder = $builder->from($table);
        return $builder;
    }

    /**
     * Returns new QueryBuilder
     *
     * @param string  $connection
     * @return DbMySqlIlluminateQueryBuilder
     */
    protected static function newBaseQueryBuilder($connection)
    {
        $conn = static::$instance->connection($connection);
        $grammar = $conn->getQueryGrammar();
        $proc = $conn->getPostProcessor();
        return new DbMySqlIlluminateQueryBuilder($conn, $grammar, $proc);
    }

    /**
     * Set PSR logger as DBAL query logger
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
        $dispatcher = new DbMySqlIlluminateDispatcher();

        // Register callback for all query events
        $dispatcher->listen('illuminate.query', function($event, $payload) {
            if (!is_string($event)) {
                $event = (string) var_export($event, true);
            }
            $this->logger->debug($event, array('payload' => $payload));
        });

        $this->getConnection()->setEventDispatcher($dispatcher);
    }

    /**
     * Return logger
     *
     * @return LoggerInterface $logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Return genral log object
     *
     * @staticvar DbMySqlGeneralLog $log
     * @return DbMySqlGeneralLog
     */
    public function getGeneralLog()
    {
        static $generalLog = null;
        if ($generalLog === null) {
            $pdo = $this->getConnection()->getPdo();
            $generalLog = new DbMySqlGeneralLog($pdo);
        }
        return $generalLog;
    }
}
