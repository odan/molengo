<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Db;

use PDO;
use Molengo\Db\DbMySqlGeneralLog;

/**
 * DbMySqlGeneralLog
 */
class DbMySqlGeneralLog
{

    /** @var PDO */
    protected $pdo = null;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Start sql logging
     *
     * @param bool $clear
     * @return DbMySqlGeneralLog
     */
    public function start($clear)
    {
        if ($clear) {
            $this->clear();
        }

        $this->pdo->exec("SET NAMES utf8;");
        $this->pdo->exec("SET GLOBAL log_output = 'TABLE';");
        $this->pdo->exec("SET GLOBAL general_log = 'ON';");
        return $this;
    }

    /**
     * Stop logging
     *
     * @param bool $clear
     * @return DbMySqlGeneralLog
     */
    public function stop($clear)
    {
        if ($clear) {
            $this->clear();
        }
        $this->pdo->exec("SET GLOBAL general_log = 'OFF';");
        return $this;
    }

    /**
     * Clear general loggs
     *
     * @return DbMySqlGeneralLog
     */
    public function clear()
    {
        $this->pdo->exec("TRUNCATE TABLE mysql.general_log;");
        return $this;
    }

    /**
     * Return status
     *
     * @return bool
     */
    public function status()
    {
        $sql = "SHOW GLOBAL VARIABLES LIKE 'general_log';";
        $result = $this->pdo->query($sql)->fetchAll(PDO::FETCH_OBJ);
        $result = strtolower($result[0]->value) === 'on';
        return $result;
    }

    /**
     * Number of executed queries
     *
     * @return int
     */
    public function count()
    {
        $sql = 'SELECT COUNT(*) FROM mysql.general_log;';
        $result = (int) $this->pdo->query($sql)->fetchColumn(0);
        return $result;
    }

    /**
     * Fetch all executed SQL queries with details
     *
     * @return array
     */
    public function all()
    {
        $sql = 'SELECT * FROM mysql.general_log;';
        $result = $this->pdo->query($sql)->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    /**
     * Fetch all executed SQL queries
     *
     * @return array
     */
    public function sql()
    {
        $result = array();
        $rows = $this->all();
        foreach ($rows as $row) {
            $result[] = $row->argument;
        }
        return $result;
    }

    /**
     * Fetch all DMS queries from server
     *
     * @return array
     */
    public function dml()
    {
        $sql = "SELECT *
          FROM
            mysql.general_log
          WHERE command_type = 'Query'
            AND (
              argument LIKE 'ALTER%'
              OR argument LIKE 'CREATE%'
              OR argument LIKE 'DROP%'
              OR argument LIKE 'RENAME%'
              OR argument LIKE 'DELIMITER%'
            );";

        $result = $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
