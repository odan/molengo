<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Db;

use Illuminate\Contracts\Events\Dispatcher;

/**
 * DbMySqlIlluminateDispatcher (Dispatcher)
 */
class DbMySqlIlluminateDispatcher implements Dispatcher
{

    /**
     * Events
     *
     * @var array
     */
    protected $events = array();

    /**
     * Current event
     *
     * @var string
     */
    protected $currentEvent = '';

    /**
     * Register an event listener with the dispatcher.
     *
     * @param  string|array  $events
     * @param  mixed  $listener
     * @param  int  $priority
     * @return void
     */
    public function listen($events, $listener, $priority = 0)
    {
        if (is_array($events)) {
            foreach ($events as $event) {
                $this->events[$event][] = $listener;
            }
        } else {
            $this->events[$events][] = $listener;
        }
    }

    /**
     * Determine if a given event has listeners.
     *
     * @param  string  $eventName
     * @return bool
     */
    public function hasListeners($eventName)
    {
        return isset($this->events[$eventName]);
    }

    /**
     * Register an event and payload to be fired later.
     *
     * @param  string  $event
     * @param  array  $payload
     * @return void
     */
    public function push($event, $payload = [])
    {

    }

    /**
     * Register an event subscriber with the dispatcher.
     *
     * @param  object|string  $subscriber
     * @return void
     */
    public function subscribe($subscriber)
    {

    }

    /**
     * Fire an event until the first non-null response is returned.
     *
     * @param  string  $event
     * @param  array  $payload
     * @return mixed
     */
    public function until($event, $payload = [])
    {

    }

    /**
     * Flush a set of pushed events.
     *
     * @param  string  $event
     * @return void
     */
    public function flush($event)
    {

    }

    /**
     * Fire an event and call the listeners.
     *
     * @param string $event
     * @param mixed $payload
     * @param bool $halt
     * @return array|null
     */
    public function fire($event, $payload = [], $halt = false)
    {
        if (!$this->hasListeners($event)) {
            return null;
        }
        $this->currentEvent = $event;
        $result = null;
        foreach ($this->events[$event] as $callback) {
            $result = $callback($event, $payload, $halt);
        }

        $this->currentEvent = '';
        return $result;
    }

    /**
     * Get the event that is currently firing.
     *
     * @return string
     */
    public function firing()
    {
        return $this->currentEvent;
    }

    /**
     * Remove a set of listeners from the dispatcher.
     *
     * @param  string  $event
     * @return void
     */
    public function forget($event)
    {
        unset($this->events[$event]);
    }

    /**
     * Forget all of the queued listeners.
     *
     * @return void
     */
    public function forgetPushed()
    {

    }
}
