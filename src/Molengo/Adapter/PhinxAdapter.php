<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Adapter;

use Phinx\Db\Adapter\MysqlAdapter;
use PDO;
use PDOException;

/**
 * PhinxAdapter
 */
class PhinxAdapter extends MysqlAdapter
{

    /** PDO */
    protected $pdo;

    /**
     * Set PDO object for adapter
     *
     * @param PDO $pdo
     * @return void
     */
    public function setPdo(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * {@inheritdoc}
     */
    public function connect()
    {
        if ($this->connection !== null) {
            return;
        }
        try {
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $options = $this->getOptions();

            // Set database name
            $dbName = $this->pdo->query('select database()')->fetchColumn();
            $this->setOptions(array('name' => $dbName));

            // Set pdo attributes
            foreach ($options as $key => $option) {
                if (strpos($key, 'mysql_attr_') === 0) {
                    $this->pdo->setAttribute(constant('\PDO::' . strtoupper($key)), $option);
                }
            }

            $this->setConnection($this->pdo);
        } catch (PDOException $exception) {
            throw new \InvalidArgumentException(sprintf(
                'There was a problem connecting to the database: %s', $exception->getMessage()
            ));
        }
    }
}
