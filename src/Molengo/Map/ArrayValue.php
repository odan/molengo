<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Map;

/**
 * ArrayValue
 */
class ArrayValue
{

    /**
     * Get array value
     *
     * <code>
     * use Molengo\Map\ArrayValue;
     *
     * $array = array();
     * $array['key1']['key2']['key3'] = 'hello';
     * $value = ArrayValue::get($array, ['key1', 'key2', 'key3'], 'default');
     * </code>
     *
     * @param array $arr
     * @param array $keys
     * @param mixed $default
     * @return mixed
     */
    public static function get(&$arr, $keys, $default = null)
    {
        if (empty($arr)) {
            return $default;
        }
        foreach ($keys as $index) {
            if (!isset($arr[$index])) {
                break;
            }
            array_shift($keys);
            if (empty($keys)) {
                return $arr[$index];
            } else {
                return static::get($arr[$index], $keys, $default);
            }
        }
        return $default;
    }

    /**
     * Return Array element value (get value)
     *
     * @param array $arr
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function read(&$arr, $key, $default = null)
    {
        if (!isset($arr[$key])) {
            return $default;
        }
        return $arr[$key];
    }

    /**
     * Set array value using keys
     * by Mark Elliot
     *
     * @param array $arr array with data
     * @param array $keys array of keys
     * @param mixed $value value
     * @return array copy of $arr with the value set
     */
    public static function set(&$arr, $keys, $value)
    {
        // We're modifying a copy of $arr, but here
        // we obtain a reference to it. we move the
        // reference in order to set the values.
        $ref = &$arr;

        while (!empty($keys)) {
            // get next first key
            $k = array_shift($keys);

            // if $ref isn't an array already, make it one
            if (!is_array($ref)) {
                $ref = array();
            }

            // move the reference deeper
            $ref = &$ref[$k];
        }
        $ref = $value;

        // return a copy of $arr with the value set
        return $arr;
    }
}
