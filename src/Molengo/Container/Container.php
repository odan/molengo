<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */
namespace Molengo\Container;

/**
 * Lambda class
 * Call a closure that is a class variable
 *
 * <code>
 * $obj = new Container();
 *
 * $obj->test = function() {
 *   echo "Hello test!\n";
 * };
 * echo $obj->test();
 *
 * </code>
 */
class Container
{

    /**
     * JFunction
     *
     * @param string $name
     * @param mixed $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        return call_user_func_array($this->$name, $args);
    }
}
