<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Io;

use ZipArchive;

/**
 * FileSystem
 */
class Filesystem extends \Symfony\Component\Filesystem\Filesystem
{

    /**
     * Find pathnames matching a pattern (recursive)
     *
     * For more complex operations you better use the Finder Component
     * http://symfony.com/doc/current/components/finder.html
     *
     * @param string $pattern search pattern e.g. *.txt
     * @param int $flags
     * @return array
     */
    public function find($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->find($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }

    /**
     * Unzip file
     *
     * @param string $filename ZIP filename
     * @param string $destination Location where to extract the files.
     * @return boolean
     */
    public function unzip($filename, $destination)
    {
        $zip = new ZipArchive();
        $res = $zip->open($filename);
        if ($res === true) {
            $zip->extractTo($destination);
            $zip->close();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the extension of the filename
     *
     * @param string $filename
     * @return string
     */
    public function extension($filename)
    {
        return strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    }

    /**
     * Returns the basename
     *
     * @param string $filename
     * @return string
     */
    public function basename($filename)
    {
        return pathinfo($filename, PATHINFO_FILENAME);
    }

    /**
     * Returns the filename
     *
     * @param string $filename
     * @return string
     */
    public function filename($filename)
    {
        return pathinfo($filename, PATHINFO_BASENAME);
    }

    /**
     * Returns the dirname
     *
     * @param string $filename
     * @return string
     */
    public function dirname($filename)
    {
        return pathinfo($filename, PATHINFO_DIRNAME);
    }

    /**
     * Create tempfile
     *
     * @param string $extension File extension
     * @param string $path The path of the temp directory
     * @return string
     */
    public function tempfile($extension = '.tmp', $path = null)
    {
        $path = ($path === null) ? '' : $path . '/';
        $file = sprintf("%s%s%s", $path, sha1(uuid()), $extension);
        if (!file_exists($file)) {
            touch($file);
            chmod($file, 0775);
        } else {
            $file = '';
        }
        return $file;
    }
}
