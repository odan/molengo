<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Io;

use Exception;
use Molengo\Util\Str;
use Symfony\Component\HttpFoundation\Response;

/**
 * MetaFile
 */
class MetaFile
{

    /** @var string directory */
    protected $path = '';

    /** @var array keys */
    protected $keys = array();

    /** @var Response */
    protected $response = null;

    /** @var array routes */
    protected $routes = array();

    /**
     * Constructor
     *
     * @param Response $response response object
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Set temp path
     *
     * @param string $path path
     * @return void
     * @throws Exception
     */
    public function setPath($path)
    {
        if (!is_writable($path)) {
            throw new Exception('Cant write to {path}', array('path' => $path));
        }
        $this->path = $path;
    }

    /**
     * Create new file and return file key (id)
     *
     * @param string $fileName [optional]
     * @param string $content [optional]
     * @return string key
     */
    public function create($fileName = null, $content = null)
    {
        // Create new key
        $key = sha1(uuid());

        // Create data file
        $dataFilename = $this->getDataFileName($key);
        $this->touchFile($dataFilename);
        $this->write($key, $content);

        // Create info file
        $info = array(
            'filename' => $fileName,
            'datafile' => $dataFilename
        );

        $infoFilename = $this->getInfoFileName($key);
        $this->touchFile($infoFilename);
        $this->setInfo($key, $info);

        $this->keys[] = $key;
        return $key;
    }

    /**
     * Append content to file
     *
     * @param string $key
     * @param string $content
     * @return integer|false returns the number of bytes that
     * were written to the file, or false on failure.
     */
    public function write($key, $content)
    {
        $result = false;
        $dataFile = $this->getDataFileName($key);
        if (file_exists($dataFile)) {
            $result = file_put_contents($dataFile, $content, FILE_APPEND);
        }
        return $result;
    }

    /**
     * Reads entire file into a string
     *
     * @param string $key
     * @param integer $offset [optional]
     * @param integer $maxLength [optional]
     * @return string|false data or false on failure
     */
    public function read($key, $offset = null, $maxLength = null)
    {
        $dataFile = $this->getDataFileName($key);

        if (!file_exists($dataFile)) {
            return false;
        }

        if ($offset === null) {
            return file_get_contents($dataFile);
        } else {
            return file_get_contents($dataFile, false, null, $offset, $maxLength);
        }
    }

    /**
     * Delete file by key
     *
     * @param string $key
     */
    public function delete($key)
    {
        $dataFileName = $this->getDataFileName($key);
        if (file_exists($dataFileName)) {
            unlink($dataFileName);
        }
        $infoFileName = $this->getInfoFileName($key);
        if (file_exists($infoFileName)) {
            unlink($infoFileName);
        }
    }

    /**
     * Get file info by key
     *
     * @param string $key
     * @return array
     */
    public function getInfo($key)
    {
        $results = array();
        $filename = $this->getInfoFileName($key);
        if (file_exists($filename)) {
            $info = file_get_contents($filename);
            $results = Str::decodeJson($info);
        }
        return $results;
    }

    /**
     * Returns filename by key
     *
     * @param string $key
     * @return string
     */
    public function getDataFileName($key)
    {
        $file = sprintf("%s/%s.data", $this->path, $key);
        return $file;
    }

    /**
     * Returns info filename by key
     *
     * @param string $key
     * @return string
     */
    protected function getInfoFileName($key)
    {
        $file = sprintf("%s/%s.info", $this->path, $key);
        return $file;
    }

    /**
     * Set file informations
     *
     * @param string $key
     * @param array $info
     * @return boolean
     */
    public function setInfo($key, $info)
    {
        $boolReturn = false;
        $file = $this->getInfoFileName($key);
        if (file_exists($file)) {
            $boolReturn = (file_put_contents($file, Str::encodeJson($info)) !== false);
        }
        return $boolReturn;
    }

    /**
     * Create file
     *
     * @param string $filename
     * @param int $time
     */
    protected function touchFile($filename, $time = null)
    {
        if ($time === null) {
            touch($filename);
        } else {
            touch($filename, $time);
        }
        umask(0);
        chmod($filename, 0775);
    }

    /**
     * Cleanup tempfiles
     *
     * @return void
     */
    public function clean()
    {
        if (!empty($this->keys)) {
            foreach ($this->keys as $key) {
                $this->delete($key);
            }
        }
        $this->keys = array();
    }
}
