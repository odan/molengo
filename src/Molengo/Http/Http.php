<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Request represents an HTTP request
 */
class Http
{

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response
     *
     * @var Response
     */
    protected $response;

    /**
     * Constructor
     *
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Returns the url leading up to the current script.
     * Used to make the webapp portable to other locations.
     *
     * @return string uri
     */
    public function getRealBasePath()
    {
        // Get URI from URL
        $uri = $this->request->getPathInfo();

        // Detect and remove subfolder from URI
        $scriptName = $this->request->server->get('SCRIPT_NAME');

        if (isset($scriptName)) {
            $dirname = dirname($scriptName);
            $dirname = dirname($dirname);
            $len = strlen($dirname);
            if ($len > 0 && $dirname != '/' && $dirname != "\\") {
                $uri = substr($uri, $len);
            }
        }
        return $uri;
    }

    /**
     * Returns a URL rooted at the base url for all relative URLs in a document
     *
     * @param string $internalUri the route
     * @param boolean $asAbsoluteUrl return absolute or relative url
     * @return string base url for $internalUri
     */
    public function getRealBaseUrl($internalUri, $asAbsoluteUrl = true)
    {
        $scriptName = $this->request->server->get('SCRIPT_NAME');
        $internalUri = str_replace('\\', '/', $internalUri);
        $dirName = str_replace('\\', '', dirname($scriptName));
        $dirName = dirname($dirName);
        $result = $dirName . '/' . ltrim($internalUri, '/');
        $result = str_replace('//', '/', $result);

        if ($asAbsoluteUrl === true) {
            $result = $this->getHostUrl() . $result;
        }
        return $result;
    }

    /**
     * Returns current url
     *
     * @return string
     */
    public function getHostUrl()
    {
        $host = $this->request->getHost();
        $port = $this->request->getPort();
        $result = $this->request->isSecure() ? 'https://' : 'http://';
        $result .= ($port == '80') ? $host : $host . ":" . $port;
        return $result;
    }

    /**
     * Returns true if runing from localhost
     *
     * @return bool
     */
    public function isLocalhost()
    {
        $addr = $this->request->getClientIp();
        $result = isset($addr) && ($addr === '127.0.0.1' || $addr === '::1');
        return $result;
    }

    /**
     * Redirect to url
     *
     * @param string $url
     * @return RedirectResponse
     */
    public function redirect($url)
    {
        return new RedirectResponse($url);
    }

    /**
     * Redirect to base url
     *
     * @param string $internalUri
     * @return RedirectResponse
     */
    public function redirectBase($internalUri)
    {
        $url = $this->getRealBaseUrl($internalUri);
        return new RedirectResponse($url);
    }
}
