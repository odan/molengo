<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Test;

use Exception;
use PHP_CodeCoverage;
use PHP_CodeCoverage_Report_Clover;
use PHP_CodeCoverage_Report_HTML;
use PHPUnit_Framework_TestFailure;
use PHPUnit_Framework_TestResult;
use PHPUnit_Framework_TestSuite;
use ReflectionClass;

/**
 * UnitTest TestSuite
 */
class TestSuite
{

    protected $testDir;
    protected $template;
    protected $namespace;
    protected $coverage = false;
    protected $coverageDir;
    protected $blacklistDirs = array();
    protected $whitelistDirs = array();

    /**
     * Set test directory with *Test.php files
     *
     * @param string $testDir
     * @return void
     */
    public function setTestDir($testDir)
    {
        $this->testDir = $testDir;
    }

    /**
     * Set test namespace
     *
     * @param string $namespace
     * @return void
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * Set HTML-Template directory for HTML output
     *
     * @param string $template
     * @return void
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Enable code coverage
     *
     * @param bool $coverage
     * @return void
     */
    public function setCoverage($coverage)
    {
        $this->coverage = $coverage;
    }

    /**
     * Set code coverage output directory
     *
     * @param string $coverageDir
     * @return void
     */
    public function setCoverageDir($coverageDir)
    {
        $this->coverageDir = $coverageDir;
    }

    /**
     * Add dir to whitelist
     *
     * @param string $dir
     */
    public function addDirectoryToWhitelist($dir)
    {
        $this->whitelistDirs[] = $dir;
    }

    /**
     * Add dir to blacklist
     *
     * @param string $dir
     */
    public function addDirectoryToBlacklist($dir)
    {
        $this->blacklistDirs[] = $dir;
    }

    /**
     * Run test suite
     *
     * @return void
     */
    public function run()
    {
        $vars = array();

        $suite = new PHPUnit_Framework_TestSuite();


        // load all tests
        $this->loadAllTests($suite);

        // Coverage
        $phpCodeCoverage = null;
        if ($this->coverage === true) {
            $phpCodeCoverage = $this->startCoverage();
        }

        // Run tests
        /* @var $result PHPUnit_Framework_TestResult */
        $result = $suite->run();

        if ($this->coverage === true && $phpCodeCoverage !== null) {
            $this->stopCoverage($phpCodeCoverage);
        }

        // Collect test results
        $tests = array();
        $this->addTestResults($result, 'errors', $tests);
        $this->addTestResults($result, 'failures', $tests);
        $this->addTestResults($result, 'passed', $tests);
        $this->addTestResults($result, 'skipped', $tests);

        // Output
        $vars['tests'] = $tests;
        $vars['coverage'] = $this->coverage;
        $vars['timeTotal'] = $result->time();

        $this->renderHtml($vars);
    }

    /**
     * Load all tests
     *
     * @param PHPUnit_Framework_TestSuite $suite
     */
    public function loadAllTests(PHPUnit_Framework_TestSuite $suite)
    {
        foreach (glob($this->testDir . '/*Test.php') as $tc) {
            $className = $this->namespace . basename($tc, '.php');
            $suite->addTestSuite(new ReflectionClass($className));
        }
    }

    /**
     * Start coverage
     *
     * @return PHP_CodeCoverage
     */
    public function startCoverage()
    {
        $phpCodeCoverage = null;

        $fs = new \Molengo\Io\Filesystem();
        if ($fs->exists($this->coverageDir)) {
            $fs->remove($this->coverageDir);
        }

        $fs->mkdir($this->coverageDir);
        $phpCodeCoverage = new PHP_CodeCoverage();
        $phpCodeCoverage->setAddUncoveredFilesFromWhitelist(true);
        $filter = $phpCodeCoverage->filter();

        foreach ($this->whitelistDirs as $dir) {
            $filter->addDirectoryToWhitelist($dir);
        }
        foreach ($this->blacklistDirs as $dir) {
            $filter->addDirectoryToBlacklist($dir);
        }

        $phpCodeCoverage->start('all');

        return $phpCodeCoverage;
    }

    /**
     * Stop coverage
     *
     * @param PHP_CodeCoverage $phpCodeCoverage
     */
    public function stopCoverage(PHP_CodeCoverage $phpCodeCoverage)
    {
        $phpCodeCoverage->stop(true);

        $writer = new PHP_CodeCoverage_Report_Clover;
        $writer->process($phpCodeCoverage, $this->coverageDir . '/clover.xml');

        $writer = new PHP_CodeCoverage_Report_HTML;
        $writer->process($phpCodeCoverage, $this->coverageDir);
    }

    /**
     * Mapping
     *
     * @param PHPUnit_Framework_TestResult $result
     * @return PHPUnit_Framework_TestSuite
     */
    protected function mapResultsToObjects(PHPUnit_Framework_TestResult $result)
    {
        $testSuites = array();
        $topTests = $result->topTestSuite();
        $topTestsTests = $topTests->tests();

        foreach ($topTestsTests as $testTemp) {
            $topTests = $testTemp->tests();
            foreach ($topTests as $tt) {
                $testName = get_class($tt) . '::' . $tt->getName();
                $testSuites[$testName] = $tt;
            }
        }
        return $testSuites;
    }

    /**
     * Add test results
     *
     * @param PHPUnit_Framework_TestResult $result
     * @param string $type
     * @param array $results
     * @return void
     * @throws Exception
     */
    protected function addTestResults(PHPUnit_Framework_TestResult $result, $type, array &$results)
    {
        $tests = $result->{$type}();
        if (empty($tests)) {
            return;
        }

        $class = ($type === 'passed') ? 'success' : 'danger';
        $class = ($type === 'skipped') ? 'warning' : $class;
        $status = ($type === 'passed') ? true : false;
        $testSuites = $this->mapResultsToObjects($result);

        foreach ($tests as $classMethodName => $testElement) {
            if (empty($testElement)) {
                continue;
            }
            $results[] = $this->getTestResult($testElement, $status, $class, $testSuites, $classMethodName);
        }
    }

    /**
     * Create test result array
     *
     * @param PHPUnit_Framework_TestFailure|array $testElement
     * @param bool $status test status
     * @param string $class cssclass
     * @param array $testSuites
     * @param string $classMethodName class name
     * @return array
     * @throws Exception
     */
    protected function getTestResult($testElement, $status, $class, $testSuites, $classMethodName)
    {
        $result = array();
        if ($testElement instanceof PHPUnit_Framework_TestFailure) {
            $test = $testElement->failedTest();
            $classMethodName = get_class($test) . '::' . $test->getName();
            $message = $testElement->getExceptionAsString();
        } else {
            if (isset($testSuites[$classMethodName])) {
                $test = $testSuites[$classMethodName];
            } else {
                throw new Exception('TestSuite not found');
            }
            $message = 'passed';
        }

        $result['name'] = $classMethodName;
        $result['status'] = $status;
        $result['class'] = $class;
        $result['time'] = $test->getTime() . ' sec.';
        $result['message'] = $message;
        $result['memory'] = $test->getMemoryStatistic();
        $result['memory_html'] = $this->getMemoryStatistcHml($test->getMemoryStatistic());
        return $result;
    }

    /**
     * Returns memory statistic as HTML
     *
     * @param array $memoryStatistic
     * @return string
     */
    protected function getMemoryStatistcHml($memoryStatistic)
    {
        if (empty($memoryStatistic)) {
            return '';
        }

        $results = array();
        $tpl = '<span class="label label-{label}">{info} {memory_text}</span>';

        // Waring at 10mb
        $warningSize = 32 * 1024 * 1024;
        $dangerSize = 64 * 1024 * 1024;

        foreach ($memoryStatistic as $mem) {
            $label = 'success';

            if ($mem['memory'] > $warningSize) {
                $label = 'warning';
            }
            if ($mem['memory'] > $dangerSize) {
                $label = 'danger';
            }

            $html = i($tpl, array(
                'label' => $label,
                'info' => gh($mem['info']),
                'memory' => gh($mem['memory']),
                'memory_text' => gh($mem['memory_text'])
            ));
            $results[] = $html;
        }
        $result = implode("<br>\n", $results);
        return $result;
    }

    /**
     * Render HTML template with variables
     * @param array $vars
     */
    protected function renderHtml($vars = array())
    {
        extract($vars, EXTR_REFS);
        require $this->template;
    }
}
