<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Test;

use PHPUnit_Framework_TestCase;

/**
 * TestCase
 */
class TestCase extends PHPUnit_Framework_TestCase
{

    protected $time = 0.0;
    protected $startTime = 0.0;
    protected $memory = array();

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     * @return void
     */
    protected function setUp()
    {
        $this->startTime = microtime(true);
        parent::setUp();
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed.
     *
     * @return void
     */
    protected function tearDown()
    {
        $this->time = microtime(true) - $this->startTime;
        parent::tearDown();
    }

    /**
     * Returns elapsed time
     *
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Returns current memory usage with or without styling
     *
     * @return int
     */
    protected function getMemoryUsage()
    {
        return memory_get_usage(true);
    }

    /**
     * Returns peak of memory usage
     *
     * @return int
     */
    protected function getMemoryUsagePeak()
    {
        return memory_get_peak_usage(true);
    }

    /**
     * Append memory usage with info (optional)
     *
     * @param string $infoText
     */
    protected function addMemoryUsage($infoText = '')
    {
        $memoryUsage = $this->getMemoryUsage();
        $memoryText = $this->formatByte($memoryUsage);

        $this->memory[] = array('time' => date('Y-m-d H:i:s'),
            'name' => $this->getName(),
            'info' => $infoText,
            'memory' => $memoryUsage,
            'memory_text' => $memoryText
        );
    }

    /**
     * Returns memory statistic
     *
     * @return array
     */
    public function getMemoryStatistic()
    {
        return $this->memory;
    }

    /**
     * Clear memory statistic
     */
    protected function clearMemoryStatistic()
    {
        $this->memory = array();
    }

    /**
     * Returns formated byte as string
     *
     * @param int $bytes
     * @param string $unit
     * @param int $decimals
     * @return string
     */
    protected function formatByte($bytes, $unit = "", $decimals = 2)
    {
        $units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4,
            'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);

        $value = 0;
        if ($bytes > 0) {
            // Generate automatic prefix by bytes
            // If wrong prefix given
            if (!array_key_exists($unit, $units)) {
                $pow = floor(log($bytes) / log(1024));
                $unit = array_search($pow, $units);
            }

            // Calculate byte value by prefix
            $value = ($bytes / pow(1024, floor($units[$unit])));
        }

        // If decimals is not numeric or decimals is less than 0
        // then set default value
        if (!is_numeric($decimals) || $decimals < 0) {
            $decimals = 2;
        }

        // Format output
        $result = sprintf('%.' . $decimals . 'f ' . $unit, $value);
        return $result;
    }
}
