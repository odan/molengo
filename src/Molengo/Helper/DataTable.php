<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */
namespace Molengo\Helper;

/**
 * DataTable model helper
 */
class DataTable
{

    /**
     * Returns filtered data for datatable component
     *
     * @param array $rows
     * @param array $params
     * - options.page (default = 0)
     * - options.view (default = 'list')
     * - options.search (default = '')
     * - options.sortDirection asc (default) or desc
     * - options.sortProperty (default = '')
     * - options.sortFlag (default = 'natural')
     * - options.page (default = 0)
     * - options.pageSize (default = 10)
     * - options.filter.value (default = 'all') (reserved)
     * - options.filter.text (default = 'all') (reserved)
     * - options.filter.selected (default = true) (reserved)
     *
     * @return array|null
     */
    public function filterDataTable(array &$rows, $params)
    {
        // Search in rows
        $rows = $this->searchDataTableRows($rows, $params);
        if (empty($rows)) {
            return null;
        }

        // Sort array
        $rows = $this->sortDataTableArray($rows, $params);
        if (empty($rows)) {
            return null;
        }

        // Pagination
        $result = $this->paginateDataTableRows($rows, $params);
        return $result;
    }

    /**
     * Paginate DataTable rows
     *
     * @param array $rows
     * @param array $params
     * @return array
     */
    protected function paginateDataTableRows(array $rows, $params)
    {
        $result = array();
        $page = gv($params['options'], 'page', 0);
        $page = ($page < 0) ? 0 : $page;
        $pageSize = gv($params['options'], 'pageSize', 10);

        // Pagination result
        $count = 0;
        if (empty($rows)) {
            $page = 0;
        } else {
            $count = count($rows);
            if ($page < 1) {
                $page = 1;
            }
        }

        $pageCount = ceil($count / $pageSize);
        if ($page > $pageCount) {
            $page = $pageCount;
        }
        $pageIndex = $page - 1;
        $offset = $pageIndex * $pageSize;
        $result['count'] = $count;
        $result['page'] = $page;
        $result['pages'] = $pageCount;
        $result['start'] = '0';
        $result['end'] = '0';
        if ($count > 0) {
            $start = ($pageIndex * $pageSize);
            $end = $start + $pageSize;
            $end = ($end < $count) ? $end : $count;
            $result['start'] = $start + 1;
            $result['end'] = $end;
        }
        $result['items'] = array_slice($rows, $offset, $pageSize);
        return $result;
    }

    /**
     * Search in rows
     *
     * @param array $rows
     * @param array $params
     * @return array
     */
    protected function searchDataTableRows(array $rows, $params)
    {
        $search = gv($params['options'], 'search', '');
        if (empty($search)) {
            return;
        }
        foreach ($rows as $i => $row) {
            $boolFound = false;
            foreach ($row as $v) {
                if (stripos($v, $search) !== false) {
                    $boolFound = true;
                    break;
                }
            }
            if (!$boolFound) {
                unset($rows[$i]);
            }
        }
        return $rows;
    }

    /**
     * Sort DataTable rows
     *
     * @param array $rows
     * @param array $params
     * @return array
     */
    protected function sortDataTableArray(array $rows, $params)
    {
        // asc or desc
        $sortDirection = gv($params['options'], 'sortDirection', 'asc');
        $sortProperty = gv($params['options'], 'sortProperty', '');
        $sortFlag = gv($params['options'], 'sortFlag', 'natural');

        if (empty($sortProperty)) {
            return $rows;
        }
        $sortFlags = array(
            'natural' => SORT_NATURAL,
            'numeric' => SORT_NUMERIC,
            'regular' => SORT_REGULAR,
            'string' => SORT_STRING
        );
        $sortFlag = isset($sortFlags[$sortFlag]) ? $sortFlags[$sortFlag] : SORT_NATURAL;
        $sortOrder = ($sortDirection == 'asc') ? SORT_ASC : SORT_DESC;
        $this->sortArrayByColumn($rows, $sortProperty, $sortOrder, $sortFlag);
        return $rows;
    }

    /**
     * Sort array by key
     *
     * @param array $arr
     * @param string $col
     * @param int $sortOrder
     * @param int $sortFlag
     */
    protected function sortArrayByColumn(&$arr, $col, $sortOrder = SORT_ASC, $sortFlag = SORT_REGULAR)
    {
        $sortCol = array();
        foreach ($arr as $key => $row) {
            $sortCol[$key] = $row[$col];
        }
        array_multisort($sortCol, $sortOrder, $sortFlag, $arr);
    }
}
