<?php

/**
 * Molengo framework
 *
 * @copyright 2004-2016 odan
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */

namespace Molengo\Error;

use ErrorException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * ErrorHandler
 */
class ErrorHandler
{

    /**
     * Response
     *
     * @var Response
     */
    protected $response = null;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $log = null;

    /**
     * Constructor
     *
     * @param Response $response
     * @param LoggerInterface $logger
     */
    public function __construct(Response $response, LoggerInterface $logger)
    {
        $this->response = $response;
        $this->log = $logger;
    }

    /**
     * Exception handler for exceptions which are not within a
     * try / catch block. Terminates the current script.
     *
     * @param ErrorException $error
     * @return void
     */
    public function handleException($error)
    {
        $this->handleErrorException($error);
    }

    /**
     * Error handler for any kind of PHP errors (except fatal errors)
     *
     * @param mixed $code
     * @param string $message
     * @param string $filename
     * @param mixed $line
     * @return boolean
     */
    public function handleError($code, $message, $filename, $line)
    {
        $error = new ErrorException($message, $code, 0, $filename, $line);
        $this->handleErrorException($error);
        // true = continue script
        // false = exit script and set $php_errormsg
        return false;
    }

    /**
     * Handler for Fatal errors
     * e.g. Maximum execution time of x second exceeded
     *
     * @return void
     */
    public function handleShutdown()
    {
        $error = error_get_last();
        if (empty($error)) {
            return;
        }
        $type = $error['type'];
        if ($type & error_reporting()) {
            $message = 'Shutdown Error: ' . error_type_text($type) . ' ' . $error['message'];
            $file = $error['file'];
            $line = $error['line'];
            $error = new ErrorException($message, $type, 0, $file, $line);
            $this->handleErrorException($error);
        }
    }

    /**
     * Error Handler ErrorException object
     *
     * @param ErrorException $error
     * @return void
     */
    public function handleErrorException($error)
    {
        $code = $error->getCode();
        $codeText = error_type_text($code);
        $file = $error->getFile();
        $line = $error->getLine();
        $message = $error->getMessage();
        $trace = $error->getTraceAsString();

        $error = sprintf("Error: [%s] %s in %s on line %s.", $codeText, $message, $file, $line);
        $error .= sprintf("\nBacktrace:\n%s", $trace);
        if ($this->log) {
            $this->log->error($error);
        }

        // Check display error parameter
        $displayErrors = strtolower(trim(ini_get('display_errors')));
        if ($displayErrors == '1' || $displayErrors == 'on') {
            $this->response->setContent($error);
        }
    }
}
